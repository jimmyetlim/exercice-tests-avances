
public class ComptePersonnel extends CompteBancaire {
    public static final double MONTANT_FRAIS_DEFAUT = 1.25;
    public static final double SEUIL_AJOUT_FRAIS = 10.0;
    
    private Personne repondant;
    private Argent aucunFrais, montantFrais;
    
    public static void main (String[] args) {
        Argent argent1 = new Argent(20.0, "USD");
        Argent argent2 = new Argent(30.0, "USD");
        Client fictif = new Client("Fictif", "Dude", Personne.HOMME);
        Personne fictive = new Personne("Fictive", "Dudette", Personne.FEMME);
        ComptePersonnel compte = new ComptePersonnel(fictif, fictive, "USD");
        
        try {
            compte.deposerArgent(argent2);
            compte.retirerArgent(argent1);
            System.out.println("Nouveau montant du compte " + compte.imprimerSolde());
        } 
        catch (SoldeInsuffisantException ex) {
            System.err.println("Une erreur s'est produite dans l'operation du compte");
        }
    }
    
    public ComptePersonnel (Client proprietaire, Personne repondant, String devise) {
        super(proprietaire, devise);
        this.repondant = repondant;
        this.aucunFrais = new Argent(0.0, devise);
        this.montantFrais = new Argent(MONTANT_FRAIS_DEFAUT, devise);
    }
    
    public Personne getRepondant () {
        return repondant;
    }
    
    public void setMontantFrais (double montant) {
        if (montant < 0.0)
            throw new IllegalArgumentException("Le montant des frais ne peut etre negatif");
        montantFrais = new Argent(montant, getDevise());
    }
    
    @Override
    public void deposerArgent (Argent montantDepot) {
        if (montantDepot.estNegatif())
            throw new IllegalArgumentException("Le montant d'argent depose ne peut etre negatif");
        solde = solde.ajouter(montantDepot); // ainsi, pas de frais sur les depots
    }

    @Override
    protected Argent calculerFrais (Argent montantOperation) {
        if (montantOperation.getMontant() < SEUIL_AJOUT_FRAIS)
            return aucunFrais;
        else
            return montantFrais;
    }
}
