
public class CompteEntreprise extends CompteBancaire {
    public static final double TAUX_FRAIS = 0.02; // soit 2% de frais
    
    private double tauxInteret;

    public static void main (String[] args) {
        Argent argent1 = new Argent(200.0, "USD");
        Argent argent2 = new Argent(300.0, "USD");
        Client fictif = new Client("Fictif", "Dude", Personne.HOMME);
        CompteEntreprise compte = new CompteEntreprise(fictif, "USD", 0.0);
        
        try {
            compte.deposerArgent(argent2);
            compte.retirerArgent(argent1);
            System.out.println("Nouveau montant du compte " + compte.imprimerSolde());
        } 
        catch (SoldeInsuffisantException ex) {
            System.err.println("Une erreur s'est produite dans l'operation du compte");
        }
    }
    
    public CompteEntreprise (Client proprietaire, String devise, double tauxInteret) {
        super(proprietaire, devise);
        if (tauxInteret < 0.0)
            throw new IllegalArgumentException("Le taux d'interet ne peut etre negatif");
        this.tauxInteret = tauxInteret;
    }
    
    public double getTauxInteret () {
        return tauxInteret;
    }
    
    public Argent appliquerInteret () {
        Argent montantInteret = new Argent(getSolde() * tauxInteret, getDevise());
        solde = solde.ajouter(montantInteret);
        return montantInteret;
    }

    @Override
    protected Argent calculerFrais (Argent montantOperation) {
        return new Argent(montantOperation.getMontant() * TAUX_FRAIS, montantOperation.getDevise());
    }
}
